#!/usr/bin/env python

from __future__ import print_function
import math
import time
import sys
import os
import traceback

import labdo as ld
from PyQt5 import QtCore
from openflexure_stage import OpenFlexureStage as OFS
import numpy as np

ui_file = ld.UiFile("GUI.ui")


class MyWindowClass(ld.MainWindow, ui_file):
    timer = QtCore.QTimer()
    def __init__(self,worker,parent=None):
        ld.MainWindow.__init__(self,worker, parent)
        self.timer = QtCore.QTimer()
        moveWidgets = [self.UpBtn,self.DownBtn,self.LeftBtn,self.RightBtn,self.InBtn,self.OutBtn,self.walkBtn,self.HillBtn]
        self.externalConnect(self.timer,"timeout",self.worker.readIntensity,queuePlacement=ld.MessageEvent.MOVETOBACK)
        self.externalConnect(self.UpBtn,"pressed",self.worker.move,blockList=moveWidgets)
        self.externalConnect(self.DownBtn,"pressed",self.worker.move,blockList=moveWidgets)
        self.externalConnect(self.LeftBtn,"pressed",self.worker.move,blockList=moveWidgets)
        self.externalConnect(self.RightBtn,"pressed",self.worker.move,blockList=moveWidgets)
        self.externalConnect(self.InBtn,"pressed",self.worker.move,blockList=moveWidgets)
        self.externalConnect(self.OutBtn,"pressed",self.worker.move,blockList=moveWidgets)
        self.externalConnect(self.walkBtn,"pressed",self.worker.rasterToLight,blockList=moveWidgets)
        self.externalConnect(self.HillBtn,"pressed",self.worker.hillWalk,blockList=moveWidgets)
        self.timer.timeout.connect(self.updatePlots)
        self.timer.start(500)
        self.publishValue('Nsteps',self.nsteps)
        self.subscribeToStream('Intensity',float,self.Intensity.setValue)
        self.Plot.setMouseEnabled(x=False, y=False)
        self.line1=None
        self.I = []
        self.subscribeToStream('Intensity',float,self.I.append)
        
    def updatePlots(self):
        downsample =  len(self.I)>100
        if self.line1 is None:
            self.line1 = self.Plot.plot(range(len(self.I)),self.I,pen='r')
        else:
            self.line1.setData(range(len(self.I)),self.I,autoDownsample=downsample)
        
        

class StageWorker(ld.Worker):
    
    def setup(self):
        #TODO Make COM port configurable by input argument
        self.Stage = OFS('COM3', baudrate = 115200)
        self.GUI.led.On = True
        self.Stage.light_sensor.gain = 9876
    
    @ld.GuiCall
    def move(self,Event):
        steps = self.readValue('Nsteps')
        if Event.widget==self.GUI.UpBtn:
            self.Stage.move_rel(steps,'z')
        elif Event.widget==self.GUI.DownBtn:
            self.Stage.move_rel(-steps,'z')
        elif Event.widget==self.GUI.LeftBtn:
            self.Stage.move_rel(-steps,'x')
        elif Event.widget==self.GUI.RightBtn:
            self.Stage.move_rel(steps,'x')
        elif Event.widget==self.GUI.InBtn:
            self.Stage.move_rel(steps,'y')
        elif Event.widget==self.GUI.OutBtn:
            self.Stage.move_rel(-steps,'y')
        else:
            assert False, "This shouldn't happen!"
            
    @ld.GuiCall
    def readIntensity(self,Event):
        i=self.Stage.light_sensor.intensity
        print(i)
        self.publishToStream('Intensity',i)
    
    @ld.GuiCall
    def rasterToLight(self,Event):
        
        def rasterSide(axis,length,step,initial_time,f):
            print("raster for %d in %s"%(length,axis))
            moved = 0
            sign = math.copysign(1,length)
            length = abs(length)
            step_signed = step*sign
            intensity=self.Stage.light_sensor.intensity
            self.publishToStream('Intensity',intensity)
            print("Initial intensity is %d"%intensity)
            while(moved <= length and intensity < threshold):
                print('moving')
                self.Stage.move_rel(step_signed,axis)
                print('moved')
                stage_pos = self.Stage.position
                intensity = self.Stage.light_sensor.intensity
                t = time.time() - initial_time
                print(stage_pos)
                print(intensity)
                print(t)
                f.write("{} {} {} {} {}\n".format(t,stage_pos[0],stage_pos[1],stage_pos[2],intensity))
                self.publishToStream('Intensity',intensity)
                moved += step
            return intensity

        gains = self.Stage.light_sensor.valid_gains
        self.Stage.light_sensor.gain = gains[-1] #maximum gain to detect faint signal
        length = 500 #initial side length of spiral
        step = self.readValue('Nsteps')
        threshold = 80
        backlash = None

        intensity = self.Stage.light_sensor.intensity
        initial_time = time.time()
        with open(os.path.join("..","data",time.strftime("%Y%m%d_%H%M%S_rasterToLight.dat")),'w') as f:
            f.write("Gain: "+str(self.Stage.light_sensor.gain)+"\n")
            f.write("Initial length of spiral: "+str(length)+"\n")
            f.write("Step: "+str(step)+"\n")
            f.write("Threshold: "+str(threshold)+"\n")
            f.write("Time,X,Y,Z,Intensity\n")
            while(intensity < threshold):
                intensity = rasterSide('x',length,step,initial_time,f)
                if (intensity>threshold):
                    break
                intensity = rasterSide('z',length,step,initial_time,f)
                length = -(length+math.copysign(step,length))
                print("length now %d"%length)
            print("found light with intensity of %d"%intensity)
    
    @ld.GuiCall
    def hillWalk(self,Event):
        
        def take_data(pts, avgs, step, start_time, f):
            I = np.zeros(points)
            pos = np.zeros([points,3])
            self.Stage.move_rel(-step*math.floor(pts/2))
            for i in range(pts):
                if i>0:
                    self.Stage.move_rel(step)
                time.sleep(.1)
                intensity = []
                for j in range(avgs):
                    intensity.append(self.Stage.light_sensor.intensity)
                    time.sleep(0.1)
                intensity = np.mean(intensity)
                position = self.Stage.position
                t = time.time() - start_time
                print(t)
                print(position)
                print(intensity)
                self.publishToStream('Intensity',intensity)
                I[i] = intensity
                pos[i,:] = position
                f.write("{} {} {} {} {} {} {}\n".format(t,position[0],position[1],position[2],self.Stage.light_sensor.gain,intensity,step))
            return (I,pos)
        
        with open(os.path.join("..","data",time.strftime("%Y%m%d_%H%M%S_hillwalk.dat")),"w") as f:
            gains = self.Stage.light_sensor.valid_gains
            self.Stage.light_sensor.gain = gains[-1]
            step_size = 400
            min_step = 20
            points = 9
            avgs = 20

            f.write("Points: "+str(points)+"\n")
            f.write("Number of intensity readings for average: "+str(avgs)+"\n")
            f.write("Time,X,Y,Z,Gain,Intensity,Step\n")

            self.Stage.backlash = 256
            start_time = time.time()
            
            #three times the motion on the optical axis
            axes = [[1,0,0],[0,0,1],[0,5,0]]
            axes = [np.asarray(axis) for axis in axes]

            #This loop runs finds a maxima in all 3 axes each time iteration, it then reduces the step size,
            #Loops until the step size reaches the set minimum step size
            while True:

                #loop for all axes
                for axis in axes:
                    axis_hat = axis/np.linalg.norm(axis)
                    step = axis*step_size
                    #loop until maxima in this axis
                    while True:
                        init_pos = self.Stage.position
                        (I,pos) = take_data(points, avgs, step, start_time, f)
                        #no light
                        if np.all(I < 10):
                            try:
                                #increment gain and repeat run
                                self.Stage.light_sensor.gain = gains[gains.index(self.Stage.light_sensor.gain)+1]
                                self.Stage.move_abs(init_pos)
                                continue
                            except IndexError:
                                print('No light detected at highest gain, cannot hill walk')
                                return
                        
                        #detector saturated
                        if np.all(I > 2**15):
                            try:
                                #decriment gain and repeat run
                                self.Stage.light_sensor.gain = gains[gains.index(self.Stage.light_sensor.gain)-1]
                                self.Stage.move_abs(init_pos)
                                continue
                            except IndexError:
                                print('Detector saturated at lowest gain!')
                                return

                        dist = np.linalg.norm(pos-pos[0],axis=1)
                        coef= np.polyfit(dist,I, 2)
                        print(dist)
                        print(coef)
                        
                        if coef[0] < 0:
                            d_max= -coef[1]/(2*coef[0])
                            print('dmax is %f'%d_max)

                            if d_max<0:
                                print("Found a curve, but was before the start")
                                self.Stage.move_rel(-axis_hat*dist[-1])
                            elif d_max>dist[-1]:
                                print("Found a curve, but was after the end")
                            else:
                                print("Look I just found the peak")
                                pos_to_be = -(coef[1] / (2 * coef[0]))
                                self.Stage.move_abs(init_pos+axis_hat*d_max)
                                break
                        else:
                            coef= np.polyfit(dist,I, 1)
                            print(coef)
                            if coef[0] > 0:
                                print("straight line with positive gradient")
                            else:
                                print("straight line with negative gradient")
                                self.Stage.move_rel(-axis_hat*dist[-1])
                
                #Reduce step size - and exit if below minium step size
                step_size = np.rint(step_size / 2)
                if step_size > min_step:
                    print("Step size reduced to %d" %step_size)
                else:
                    f.close()
                    break
    
    

if __name__ == "__main__":
    with StageWorker() as worker:
        app = ld.App(MyWindowClass, worker, sys.argv)
        app.run()

    # This does actually work! It's backwards comaptible.
    # Joel was just being a knob, and forgot some stuff in labdo
    #app = ld.App(MyWindowClass, StageWorker(), sys.argv)
    #app.run()